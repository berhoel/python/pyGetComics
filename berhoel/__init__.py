"""Initialization for berhoel python namespace packages."""

from __future__ import annotations

__date__ = "2024/07/12 22:55:41 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2017, 2022 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

__path__ = __import__("pkgutil").extend_path(__path__, __name__)
