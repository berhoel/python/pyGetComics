import abc
import enum
from abc import ABC, abstractmethod
from pathlib import Path

from _typeshed import Incomplete
from sphinx.application import Sphinx

__all__ = [
    "ProjectTypes",
    "configuration",
    "extensions",
    "templates_path",
    "language",
    "exclude_patterns",
    "html_theme",
    "html_theme_path",
    "html_static_path",
    "latex_elements",
    "latex_show_urls",
    "latex_engine",
    "intersphinx_mapping",
    "todo_include_todos",
    "napoleon_google_docstring",
    "napoleon_numpy_docstring",
    "napoleon_include_init_with_doc",
    "napoleon_include_private_with_doc",
    "napoleon_include_special_with_doc",
    "napoleon_use_admonition_for_examples",
    "napoleon_use_admonition_for_notes",
    "napoleon_use_admonition_for_references",
    "napoleon_use_ivar",
    "napoleon_use_param",
    "napoleon_use_rtype",
    "autodoc_default_options",
    "favicons",
    "get_html_theme_path",
    "setup",
    "sitemap_url_scheme",
]

class ProjectTypes(enum.Enum):
    POETRY: Incomplete
    CMAKE: Incomplete

class _ProjectInfo(ABC, metaclass=abc.ABCMeta):
    @property
    @abstractmethod
    def project(self) -> str: ...
    @property
    @abstractmethod
    def release(self) -> str: ...
    @property
    @abstractmethod
    def author(self) -> str: ...
    def configuration(self) -> dict[str, str]: ...

class PyProject_toml(_ProjectInfo):
    def __init__(self, cfg_file: Path | str | None = None) -> None: ...
    @property
    def project(self) -> str: ...
    @property
    def release(self) -> str: ...
    @property
    def author(self) -> str: ...
    @property
    def html_baseurl(self) -> str: ...

class CMakeLists_txt(_ProjectInfo):
    def __init__(self, cfg_file: Path | str | None = None) -> None: ...
    @property
    def project(self) -> str: ...
    @property
    def release(self) -> str: ...
    @property
    def author(self) -> str: ...

def configuration(
    project_type: ProjectTypes = ...,
    cfg_file: Path | str | None = None,
) -> _ProjectInfo: ...
def get_html_theme_path() -> list[Path]: ...

extensions: Incomplete
templates_path: Incomplete
language: str
exclude_patterns: Incomplete
html_theme: str
html_theme_path: Incomplete
html_static_path: Incomplete
latex_elements: Incomplete
latex_show_urls: str
latex_engine: str
intersphinx_mapping: Incomplete
todo_include_todos: bool
napoleon_google_docstring: bool
napoleon_numpy_docstring: bool
napoleon_include_init_with_doc: bool
napoleon_include_private_with_doc: bool
napoleon_include_special_with_doc: bool
napoleon_use_admonition_for_examples: bool
napoleon_use_admonition_for_notes: bool
napoleon_use_admonition_for_references: bool
napoleon_use_ivar: bool
napoleon_use_param: bool
napoleon_use_rtype: bool
autodoc_default_options: Incomplete
favicons: Incomplete

def setup(app: Sphinx) -> None: ...

sitemap_url_scheme: str
