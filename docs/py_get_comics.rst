..
  Task: Documentation of get_comics

  :Authors:
     - Berthold Höllmann <berhoel@gmail.com>__
  :Organization: Berthold Höllmann
  :datestamp: %Y-%m-%d
  :Copyright: Copyright © 2022 by Berthold Höllmann

CLI interfaces
==============

.. sphinx_argparse_cli::
   :module: berhoel.get_comics
   :func: get_parser
   :prog: get_comics

.. sphinx_argparse_cli::
   :module: berhoel.get_comics.peanuts
   :func: get_parser
   :prog: get_peanuts

.. sphinx_argparse_cli::
   :module: berhoel.get_comics.garfield
   :func: get_parser
   :prog: get_garfield


..
  Local Variables:
  mode: rst
  compile-command: "make html"
  coding: utf-8
  End:
