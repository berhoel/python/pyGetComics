.. include:: ../README.rst

API documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:

   get_comics

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   py_get_comics
   _tmp/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
